/*!

=========================================================
* BLK Design System React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/blk-design-system-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/blk-design-system-react/blob/main/LICENSE.sm)

* Coded by Creative Tim
 .
  
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Sofre.

*/ 
import React from 'react'
import Countdown from 'react-countdown';
import Fade from 'react-reveal/Fade';
import { fadeIn } from 'react-animations'
import { ethers } from 'ethers'
import abi from './ContractAbi.json';
import classnames from 'classnames' 
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from 'perfect-scrollbar'
import StarfieldAnimation from 'react-starfield-animation'
import  { useEffect, useState } from  'react'
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Label,
  FormGroup,
  Form,
  Input,
  FormText,
  NavItem,
  NavLink,
  Nav,
  Table,
  TabContent,
  TabPane,
  Container,
  Row,
  Col,
  UncontrolledTooltip,
  UncontrolledCarousel
} from 'reactstrap'

// css
import './style.css'

// core components
import ExamplesNavbar from 'components/Navbars/ExamplesNavbar.js'
import Footer from 'components/Footer/Footer.js'



const carouselItems = [
  {
    src: "https://i.imgur.com/pWap3Z1.png", 
    altText: 'Slide 1',
  },
  {
    src: "https://i.imgur.com/SnY3VsQ.png", 
    altText: 'Choose Your Fraction',
    
  },
  {
    src: "https://i.imgur.com/B1YrC3L.jpg",
    altText: 'My Galaxy ',

  },
  {
    src: "https://i.imgur.com/j1uywDB.png",
    altText: 'Slide 3',

  }
]

let ps = null

export default function ProfilePage() {
  const [tabs, setTabs] = React.useState(1)
  React.useEffect(() => {
    if (navigator.platform.indexOf('Win') > -1) {
      document.documentElement.className += ' perfect-scrollbar-on'
      document.documentElement.classList.remove('perfect-scrollbar-off')
      let tables = document.querySelectorAll('.table-responsive')
      for (let i = 0; i < tables.length; i++) {
        ps = new PerfectScrollbar(tables[i])
      }
    }
    document.body.classList.toggle('profile-page')
    // Specify how to clean up after this effect:
    return function cleanup() {
      if (navigator.platform.indexOf('Win') > -1) {
        //ps.destroy();
        document.documentElement.className += ' perfect-scrollbar-off'
        document.documentElement.classList.remove('perfect-scrollbar-on')
      }
      document.body.classList.toggle('profile-page')
    }
  }, [])

  
const contractAddress = "0x45ACe6928a7Ac637791Ef55FccEb0ebA950E9628";
  let contract;
  let signer;
  let signerAddress;
  let provider;

  useEffect(() => {
    async function blockchainInit() {
      if (typeof window.ethereum !== 'undefined') { 
        const provider = new ethers.providers.Web3Provider(window.ethereum, 'any');
        await provider.send("eth_requestAccounts", []);
        signer = await provider.getSigner();
        signerAddress = await signer.getAddress();
        console.log(mintAmount);
        contract = new ethers.Contract(contractAddress, abi, provider)
      }
      else {
        showAlert("Please Install Metamask!");
      }
    }

    blockchainInit();
   
  })

    const [mintAmount, setMintAmount] = useState('');
    const [tokenId, setTokenID] = useState('');
    const [cost, setCost] = useState('');
    const [baseURI, setBaseURI] = useState('');
    const [baseExtension, setBaseExtension] = useState('');

    const [from, setFromAddress] = useState('');
    const [to, setToAddress] = useState('');
    const [tokenid, set_tokenid] = useState('');
    const [token_id, setOwnerTokenId] = useState('');
    const [balanceOfAddress, setBalanceOfAddress] = useState('');
    const [WalletOfAddress, setWalletOfAddress] = useState('');

    function showAlert(err) {
   alert(err)
    }

    function showAlertSuccess(data) {
      alert('success ')
    }

  
  

async function mint() {

try {
let cost = await contract.cost();
cost = ethers.utils.formatEther(cost) * mintAmount;
const data = await contract.connect(signer).mint(signerAddress, mintAmount,
{value: ethers.utils.parseEther(cost.toString())});
} catch (err) {
showAlert(err);
}

}
    
  async function setCostValue() {
      try {
        const data = await contract.connect(signer).setCost(ethers.utils.parseEther(cost));
        showAlertSuccess(data)
      } catch (err) {
        showAlert(err);
      }
    }    

 
  async function setBaseURIValue() {
    try {
      const data = await contract.connect(signer).setBaseURI(baseURI);
      console.log('data: ', data.toString())
    } catch (err) {
      showAlert(err);
    }
  }

  async function setBaseExtensionValue() {
    try {
      const data = await contract.connect(signer).setBaseExtension(baseExtension);
      console.log('data: ', data.toString())
    } catch (err) {
      showAlert(err);
    }
  }

  async function withdrawFunds() {
    try {
      const data = await contract.connect(signer).withdraw();
      console.log('data: ', data.toString())
    } catch (err) {
      showAlert(err);
    }
  }

  async function transferFrom() {
    try {
      const data = await contract.connect(signer).transferFrom(from, to, tokenid);
      console.log('data: ', data.toString())
    } catch (err) {
      showAlert(err);
    }
  }

  // Getters

  async function name() {
    try {
      const data = await contract.name();
      showAlertSuccess(data);
    } catch (err) {
      showAlert(err);
    }
  }

  async function symbol() {
    try {
      const data = await contract.symbol();
      showAlertSuccess(data);
    } catch (err) {
      showAlert(err);
    }
  }

  async function totalSupply() {
    try {
      const data = await contract.totalSupply();
      showAlertSuccess(data);
    } catch (err) {
      showAlert(err);
    }
  }

  async function owner() {
    try {
      const data = await contract.owner();
      showAlertSuccess(data);
    } catch (err) {
      showAlert(err);
    }
  }

  async function totalSupply() {
    try {
      const data = await contract.totalSupply();
      showAlertSuccess(data);
    } catch (err) {
      showAlert(err);
    }
  }

  
  async function ownerOf() {
    try {
      const data = await contract.ownerOf(token_id);
      showAlertSuccess(data);
    } catch (err) {
      showAlert(err);
    }
  }
  
  async function balanceOf() {
    try {
      const data = await contract.balanceOf(balanceOfAddress);
      showAlertSuccess(data);
    } catch (err) {
      showAlert(err);
    }
  }

  async function walletOfOwner() {
    try {
      const data = await contract.walletOfOwner(WalletOfAddress);
      showAlertSuccess(data);
      console.log('data: ', data.toString())
      
    } catch (err) {
      showAlert(err);
    }
}

async function tokenURI() {
    try {
      const data = await contract.tokenURI(tokenId);
      showAlertSuccess(data)
    } catch (err) {
      showAlert(err);
    } 
}
  return (
    <>
      <ExamplesNavbar />
     
      <div className='wrapper'>
    
        <div className='page-header'>
          <img
            alt='...'
            className='dots'
            src={require('assets/img/dots.png').default}
          />
          <img
            alt='...'
            className='path'
            src={require('assets/img/path4.png').default}
          />
          <Container className='align-items-center'>
         
            <StarfieldAnimation
              style={{
                position: 'absolute',
                width: '100%',
                height: '100%'
              }}
            />
            <Row>
              <Col lg='6 order-2 order-lg-0' md='6 order-2 order-lg-0'>
                <h1 className='profile-title text-left' id="dyk" style={{fontsize: "3.4em",
    wordspacing: "10px",
    letterspacing:" 10px"}}>
                 Kosmo  <span>Aliens </span>
               
                </h1>
              
                <h5 className='text-on-back'>01</h5>
                
              
                <p className='profile-description'>
               Kozmo Aliens is a a brand new emerging Sci-FI NFT Play To Earn Strategy game that takes strategy and blockchain games  to the next level. 
               As the <strong> leader </strong> of your civilization, will you impose your vision and build the greatest stellar empire?
               
                </p>
              

               
              </Col>
              <Col className='ml-auto mr-auto' lg='4' md='6'>
                <img
                  alt='...'
                  className='shapes circle'
                  src={require('assets/img/chester-wade.jpg').default}
                />
              </Col>
            </Row>
          </Container>
        </div>

        <div id='sections' style={{ backgroundcolor: 'red' }}>
          <Container>
            <Row className='justify-content-between'>
              <Col md='6'>
                <Row className='justify-content-between align-items-center'>
                  <UncontrolledCarousel items={carouselItems} />
                </Row>
              </Col>
              <Col md='5'>
                <h1 className='profile-title text-left'>Introducing Kozmo Aliens  Play to earn Game  </h1>
                <h5 className='text-on-back'>02</h5> <br/>
                <p className='profile-description text-left'>
                 <span> Fight For Resources  </span> Dive into the <span> Kozmo Aliens Universe </span> through a grand strategy game of space exploration, territorial conquest, political domination,  and more!
                  
                </p>
                <div className='btn-wrapper pt-3 d-inline-block'> <br/>
                  <Button
                    className='btn-simple'
                    id="b"
                    color='info'
                    href='https://discord.gg/xRN4MtsfVN'
                    style={{top:"30px", left : "100px"}}
                    onClick={e => e.preventDefault()}
                  > 
                    <i className='fab fa-discord' /> Join The Communitiy 
                  </Button>
              
                </div>
              </Col>
            </Row>
          </Container>
        </div>

        

        <section className='section' id='crap-roadmap'>
          
          <Container>
            
            <Row>
              
           
              <Col md='7 order-2 order-lg-0'>
              
                <Row>
                  <Col sm='2'>
                    <span class='pointz-num'>Phase 1   </span>
                  </Col>
                  <Fade left>
                  <Col sm='10'> 
                    <span class='pointz-des'><br/><br/><br/><br/>
                    <strong> Journey Begins </strong><br/><br/>
                    <strong>  December 2021   </strong> <br/><br/>
                  
                    Game Design + Development and Game Testing 
                    <br/><br/> Character , Planet and Spaceship design plus  NFT system integration. 
                    
                  

   
                    </span>
                  </Col>
                  </Fade>
                </Row>

                <div class='vline'></div>

                <Row>
                  <Col sm='2'>
                    <span class='pointz-num'>Phase 2   </span>
                  </Col>
                  <Fade left>
                  <Col sm='10'>
                 
                    <span class='pointz-des'>
                  <strong>Q1 - Q2  2022 <br/><br/>   $Kozmo Token IDO  + Game Demo</strong>
                    <br></br> A BEP-20 utility token will be launched that will be freely tradable on pancakeswap.  These tokens allow you to rename and upgrade
                  your
                   Alien leaders and write their own backstory, stored in the metadata on the blockchain.<br/><br/>
 you will also be able to use these tokens to invade other galaxies and upgrade your spaceship , planet and  Kozmo alien.

                    </span>
                   
                  </Col>
                  </Fade>
                </Row>

                <div class='vline'></div>

                <Row>
                  <Col sm='2'>
                    <span class='pointz-num'>Phase 3   </span>
                  </Col>
                <Fade left>

                  <Col sm='10'>
                    <span class='pointz-des'><strong> Q3 2022  <br></br> Release of Kozmo Aliens Alpha </strong> <br/><br/>
               Expected launch date of the Kozmo Aliens gaming platform 
               You can expect to evolve your Kozmo Aliens and SpaceShips, engage in PvP battles and begin building your own galactic empire! 
                    </span>
                  </Col>
                  </Fade>
                </Row>

                <div class='vline'></div>

                <Row>
                  <Col sm='2'>
                    <span class='pointz-num'>Phase 4  </span>
                  </Col>
                  <Fade left>
                  <Col sm='10'>
                    <span class='pointz-des'>
                    <strong> Q3 2022  <br></br> SpaceShip Airdrops + Second Update </strong> <br/><br/>

                    Kozmo space holders will be able to mint their exclusive 1/1 spaceship. 
                   Second Update of the space atlas game will be released , which will include 
                   improved battles , graphics , planets to explore and more units to build
                    </span>
                  </Col>
                  </Fade>
                </Row>

                <div class='vline'></div>

                <Row>
                  <Col sm='2'>
                    <span class='pointz-num'>Phase 5  </span>
                  </Col>
                  <Fade left>
                  <Col sm='10'>
                    <span class='pointz-des'>
                    <strong>Q1 2023  <br></br> Release of Kozmo Alien Community and Third Update</strong><br/><br/>
                  Launch of the third update of the Kozmo Aliens game, 
                  which will include PvE single player mode , a mini game , the Kozmo Alien marketplace, farming, and occupations. <br/>
                    </span><br/><br/>
                  </Col>
                  </Fade>
                </Row>
             
              </Col>
            
              <Col md='5'>
                
                <h1 className='roadmap-text'>Roadmap Activations</h1>
                <br/>
              </Col>
             
            </Row>
          </Container>
        </section>
        <section className="section"> <br/> <br/> <br/>
        <Fade left>
          <h1 className="fwa" style={{textAlign:"center", fontSize:"50px"}}> Partners and backers </h1><br/><br/><br/>
          </Fade>
          
        <div class="row"><br/>
  <div class="col-6 col-sm-4"><img id="k"src="https://www.solchicks.io/wp-content/uploads/2021/11/VC-MASTER-VENTURES.png" style={{marginleft: "-50px"}}/></div> <br/>
  <div class="col-6 col-sm-4"><img  id="gts" style={{marginleft:"-20px"}}src="https://www.solchicks.io/wp-content/uploads/2021/11/II-GTS.png"/></div><br/>
  <div class="col-6 col-sm-4"><img id="wj" src="https://www.solchicks.io/wp-content/uploads/2021/11/II-whatoplay.png"/></div><br/>
  <div class="col-6 col-sm-4"><img id="b" src="https://www.solchicks.io/wp-content/uploads/2021/10/Blockchain.News-logo_black_bbg.png" style={{height: " 70px" , 
    margintop: "17px" }}/></div><br/>
  


  <div class="col-6 col-sm-4"><img  id="m" src="https://www.solchicks.io/wp-content/uploads/2021/11/CM-FFventures.png"/></div><br/>
  <div class="col-6 col-sm-4"><img id= "c" src="https://www.solchicks.io/wp-content/uploads/2021/10/coindesk-logo.svg"/></div><br/>
  <div class="col-6 col-sm-4"><img id="cu" src="https://www.solchicks.io/wp-content/uploads/2021/11/curate_full_logo_280x66.png"/></div>
  <div class="col-6 col-sm-4"><img id="m" src="https://metaverse.properties/wp-content/uploads/2020/09/Decentraland-1024x374.png"/></div><br/>
  <div class="col-6 col-sm-4"><img id="fd" src=" https://www.solchicks.io/wp-content/uploads/2021/09/altura.png"/> </div><br/>
  </div>
</section>
        <section id="n">
        <section id="n" class="speakers-section" style= {{backgroundImage: `url("https://i.ibb.co/92HJxz2/team-bg.jpg")`}}>
        <div class="parallax-scene parallax-scene-2 anim-icons">
            <span data-depth="0.40" class="parallax-layer icon icon-circle-5"></span>
            <span data-depth="0.99" class="parallax-layer icon icon-circle-5"></span>
        </div>

        <div class="container">
            <div class="sec-title light text-center">
               <h1> <span class="title">Our Team</span> </h1>
                <h6>Meet the Team</h6>
            </div>

            <div class="row">
          
                <div class="speaker-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                       
                            <figure class="image"><img    src={require('assets/img/90.jpg').default}
/>                              
                            </figure>
                        </div>
                        <div class="caption-box">
                            <h4 class="name"><a href=" https://www.instagram.com/leonb_official/">Leon  </a></h4>
                            <span class="designation">CEO  <br></br> B.S. business management @NYU Stern School of Business   </span>
                        </div>
                    </div>
                </div>

           
                <div class="speaker-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img    src= "https://i.imgur.com/0a6p197.jpeg" />
                               
                            </figure>
                        </div>
                        <div class="caption-box">
                            <h4 class="name"><a href="https://www.linkedin.com/in/3dhome/">Shahidaasy</a></h4>
                            <span class="designation">Head of 3D & Motion  Department . <br></br> Former graphics designer at Leauge of Legends. </span>
                        </div>
                    </div>
                </div>
                   <div class="speaker-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img    src= "https://i.imgur.com/0a6p197.jpeg" />
                               
                            </figure>
                        </div>
                        <div class="caption-box">
                            <h4 class="name"><a href="https://www.linkedin.com/in/3dhome/">Maxwell Rose</a></h4>
                            <span class="designation">Lead Blockchain Developer <br></br>  </span>
                        </div>
                    </div>
                </div>
                
                <div class="speaker-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img    src= "https://i.imgur.com/F7eJkXm.png" />
                               
                            </figure>
                        </div>
                        <div class="caption-box">
                            <h4 class="name"><a href="https://www.linkedin.com/in/aruncherny/">Arun Cherny</a></h4>
                            <span class="designation">SMM Manager   </span>
                        </div>
                    </div>
                </div>
                     <div class="speaker-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img    src= "https://i.imgur.com/l8bThW2.png" />
                               
                            </figure>
                        </div>
                        <div class="caption-box">
                            <h4 class="name"><a href="https://i.imgur.com/2LlZEOy.jpg/">John Shirman </a></h4>
                            <span class="designation">Content Director   </span>
                        </div>
                    </div>
                </div>
         
                <div class="speaker-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="800ms">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image"><img    src="https://i.imgur.com/UgC9b7t.jpg" />
                              
                            </figure>
                        </div>
                        <div class="caption-box">
                            <h4 class="name"><a href="https://twitter.com/Nnamokooh"> Maximillian Gacek  </a></h4>
                            <span class="designation">Head of Game Development. <br></br> B.S Computer Science Yale University </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </section>
   
        <Footer />
      </div>
    </>
  )
}
